export const state = () => ({
  token: null,
  user: null,
  isUserLoggedIn: false
})

export const mutations = {
  setToken(state, token) {
    state.token = token
    if (token) {
      state.isUserLoggedIn = true
    } else {
      state.isUserLoggedIn = false
    }
  },
  setUser(state, user) {
    state.user = user
  }
}
