# WebGIS-Project

This project was developed as part of the seminar 'WebGIS' at the Master's study program of Geoinformatics at the Hafencity-University Hamburg.

## Introduction
The main goal of the seminar was the **development of a functional WebGIS** that enables the user to interact with a web-map to view and add georeferenced information on a specific topic - in this case helping each other in your neighborhood. The goal was NOT to develop a complete website featuring a design scheme and other functionality (although some of which was implemented nonetheless and might be added in the future [see chapter *Future development*]. 

ADD IMAGE!

The project is developed as a full-stack application, using **Vue.JS** / **Nuxt.JS** on the frontend, **Node.JS** / **Express** on the backend and is communicating with the **PostgreSQL + PostGIS** database(s) using the **Sequelize** ORM. The mapping functionality is built using **Vuelayers**, which is a wrapper of the **OpenLayers** library. Additionally the **OpenRouteService** API is used for geocoding, reverse geocoding and routing. Lastly, the user authentication, password encryption and verification are handled by **JSONWebToken**, **Bcrypt** and **Joi** respectively.

## Installation

### Linux
#### using Docker
Start up a PostgreSQL + PostGIS container:

    $ sudo docker run --name postgres-db -e POSTGRES_PASSWORD=123 -d --net="host" postgis/postgis

Inside the container create a *webgis* database:

    $ sudo docker exec -it postgres-db bash
    # psql -U postgres
    # CREATE DATABASE webgis;
    # \c webgis
    # CREATE EXTENSION postgis;
    # \q

Press Ctrl+D to exit the container.

Start up the NUXT.js and Node.js containers:

    $ sudo docker run --name node-server -d --net="host" niklaskemm/webgis-node-server
    
    $ sudo docker run --name nuxt-server -d --net="host" niklaskemm/webgis-nuxt-server

Make sure the containers are running:

    $ sudo docker ps -a

After a short while the application will be available at **localhost:3000**.
#### from source
Node.js + npm and PostgreSQL + PostGIS needed!

Set up a PostgreSQL + PostGIS database called *webgis* , exposed at port 5432.

Clone the source code:

    $ git clone https://gitlab.com/niklas.kemm/webgis-using-nuxt.git
    
Navigate into the *backend* folder, install dependencies and start up the Node.js server:

    $ cd webgis-using-nuxt/server/backend && npm install && npm start
    
In another window navigate back into the server folder, install dependencies and start the Nuxt.js server:

    $ cd .. && npm install && npm run dev

### Windows
coming soon...

## Functionality
### Frontend
The project features multiple pages that serve different purposes. To navigate between these an app-wide sidebar is used. This sidebar changes based on the status and permissions of the user, utilizing the state-management of the **Vuex** store (which also handles the light-, dark- and sepia-modes that the app features).

##### main page
The main page features buttons to either login to an existing account or register a new one by proving a username, email address an password. Once logged in, the user can fully navigate between the different pages using the sidebar. 

##### map page
The main component of the app is the Map-page. It features a fullscreen map with multiple baselayers, a searchbar, a button to add additional datasets as well as a button to apply filters to the shown datasets (as of right now only based on type). The map itself can be navigated using mouse drags, with clicks showing the exact location in the searchbar in Latitude, Longitude format. Additionally, a plus-icon is displayed where ever the last clicked appeared, letting the user add a dataset at the given location. Furthermore, the searchbar can be used to reverse geocode said location. It is also possible to enter any string into the searchbar, prompting the OpenRouteService to geocode the string and centering the map on it. 

The individual dataset themselves are displayed either as a marker or a linestring. These are clickable and feature a popup that displays the most important information, including the title, description, user, date and time. From here one can choose to see the user's page or additional info of the dataset. 

The button to add datasets using a form or a route is displayed on the lower left side of the map and features an overlay to choose between a form to add a either a single point or a linestring. Points will then be added by typing in the latitude and longitude, while linestrings also offer the possibility to enter addresses which will be reverse geocoded. All datasets feature a type (as of right now only mowing, shopping and others),  title, description, start date and time, with user being added automatically based on the stored user-ID.

The filter button is situated in the lower left of the map and also features an overlay which lets the user choose between the three types of datasets to display.

##### browse page
The next page down the sidebar is the Browse-page. It displays responsive cards for every dataset that is stored in the database. These cards share a lot of features with the popup on the Map-page in that the same information is displayed and the same pages are linked. But in addition to that the owner (or admin) of a dataset may choose to edit the dataset or delete it from here.

##### additional items
There also is an About-page, which is empty as of right now, as well as a logout button, which logs the current user out and links back to the main page.  
Lastly there is a settings 'menu' which allows the user to choose between a light-, dark- and sepia-color theme. 

### backend
##### database
##### 
## Known Issues

## Future development


