const Offer = require('../models').Offer
const User = require('../models').User

module.exports = {
  async index(req, res) {
    try {
      const users = await User.findAll({
        include: [Offer]
      })
      res.send(users)
    } catch (err) {
      res.status(500).send({
        // error: 'An errror has occurd trying to fetch the users'
        err
      })
    }
  },
  async getUserById(req, res) {
    try {
      const user = await User.findAll({
        where: {
          user_id: req.params.user_id
        },
        include: [Offer]
      })
      res.send(user)
    } catch (err) {
      res.status(500).send({
        // error: 'An errror has occurd trying to fetch the user'
        err
      })
    }
  },
}
