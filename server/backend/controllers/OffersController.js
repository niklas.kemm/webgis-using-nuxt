const Offer = require('../models').Offer
const User = require('../models').User
const {
  Op
} = require('sequelize')

module.exports = {
  async index(req, res) {
    try {
      const offers = await Offer.findAll({
        include: [User]
      })
      res.send(offers)
    } catch (err) {
      res.status(500).send({
        error: err
      })
    }
  },

  async getOfferById(req, res) {
    try {
      const offer = await Offer.findAll({
        where: {
          offer_id: req.params.offer_id
        },
        include: [User]
      })
      res.send(offer)
    } catch (err) {
      res.status(500).send({
        error: err
      })
    }
  },
  async post(req, res) {
    try {
      const offer = await Offer.create(req.body)
      res.send(offer)
    } catch (err) {
      res.status(500).send({
        error: err
      })
    }
  },
  async filteredOffers(req, res) {
    try {
      const filter = req.body.filter
      const type = filter.type
      const offers = await Offer.findAll({
        where: {
          category: {
            [Op.or]: type
          }
        },
        include: [User]
      })
      res.send(offers)
    } catch (err) {
      res.status(500).send({
        error: err
      })
    }
  },
  async updateOffer(req, res) {
    try {
      const offer = await Offer.update(req.body, {
        where: {
          offer_id: req.body.offer_id
        }
      })
      res.send(offer)
    } catch (err) {
      res.status(500).send({
        error: err
      })
    }
  },
  async delete(req, res) {
    try {
      await Offer.destroy({
        where: {
          offer_id: req.body.offer_id
        }
      })
      res.status(200).send()
    } catch (err) {
      res.status(500).send({
        error: err
      })
    }
  }
}
