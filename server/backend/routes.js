const AuthenticationController = require('./controllers/AuthenticationController')
const AuthenticationControllerPolicy = require('./policies/AuthenticationControllerPolicy')
const OffersController = require('./controllers/OffersController')
const UsersController = require('./controllers/UsersController')

module.exports = (app) => {
  app.post(
    '/register',
    AuthenticationControllerPolicy.register,
    AuthenticationController.register
  )

  app.post('/login', AuthenticationController.login)

  app.post('/filteredOffers', OffersController.filteredOffers)

  app.post('/updateOffer', OffersController.updateOffer)

  app.get('/offers', OffersController.index)

  app.get('/offers/:offer_id', OffersController.getOfferById)

  app.get('/users/:user_id', UsersController.getUserById)

  app.post('/offers', OffersController.post)

  app.post('/deleteOffers', OffersController.delete)

  app.delete('/offers', OffersController.delete)
}
