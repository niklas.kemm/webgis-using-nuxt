module.exports = (sequelize, DataTypes) => {
  var Offer = sequelize.define('Offer', {
    offer_id: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      primaryKey: true
    },
    category: DataTypes.STRING,
    title: DataTypes.STRING,
    description: DataTypes.TEXT,
    location: DataTypes.GEOMETRY,
    start_date: DataTypes.DATE,
    end_date: DataTypes.DATE
  })

  Offer.associate = (models) => {
    Offer.belongsTo(models.User, {
      foreignKey: "user_id"
    })
  }

  return Offer
}
