module.exports = {
  port: process.env.PORT || 8081,
  db: {
    // HOST: process.env.DB_HOST || 'localhost',
    HOST: "localhost",
    PORT: 5432,
    // USER: process.env.DB_USER || 'postgres',
    USER: "postgres",
    // PASSWORD: process.env.DB_PASS || '123',
    PASSWORD: "123",
    // DB: process.env.DB_NAME || 'webgis',
    DB: "webgis",
    // dialect: process.env.DIALECT || 'postgres',
    dialect: "postgres"
  },
  authentication: {
    jwtSecret: process.env.JWT_SECRET || 'secret'
  }
}
