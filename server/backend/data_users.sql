INSERT INTO Users (user_id,user_name,email,"isAdmin",picture,"password","createdAt","updatedAt") VALUES 
('d457fcf0-909f-408e-ae99-b93dc5310da4','Niklas Kemm','niklas.kemm@googlemail.com',false,NULL,'$2a$08$xShzKNFnvI/KiqVtbMaqTOrWrq2efKcfxVOdyCG5NAQXkcScCwpmW','2020-09-22 16:03:33.299','2020-09-22 16:03:33.299')
,('81321adb-646a-44ff-8ad6-4587a3277afd','Hannes Braun','hannesbraun@test.com',false,NULL,'$2a$08$J7csrXfX/VSHPly9wKnQZ.5jgCuc2oIfAOVWpWj4p/f.uTBymqx9u','2020-09-25 11:12:53.902','2020-09-25 11:12:53.902')
,('6b9c4231-22cd-4646-8248-90126dcce2e1','Patrick Benecken','patrickbenecken@test.com',false,NULL,'$2a$08$vqIUcER8rpWJ8vXYCnh8w.7n0sG0EscIW4r8Xz8CE7F/VPpBVPEU6','2020-09-25 11:13:36.936','2020-09-25 11:13:36.936')
,('263f7e51-d4cc-4c89-86a7-851ae716b109','Dorian Harder','dorianharder@test.com',false,NULL,'$2a$08$1oAdzuvvjQLQppq/2FVC6enRjS7eHSy6K4yRQnf3j9x.qNAaFG0py','2020-09-25 11:14:10.543','2020-09-25 11:14:10.543')
,('dee3bc0a-6e10-41cc-a5c9-d2de7cfe9534','admin','admin@test.com',true,NULL,'$2a$08$Nd.Pgc1voLnx/nEKdTwdP.BYD0JmN3w/ooR35bzAtgJxUBD46Zylu','2020-09-25 11:15:34.146','2020-09-25 11:15:34.146')
;
