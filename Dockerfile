FROM node:12

# Create app directory
WORKDIR /usr/src/app

# Install app dependencies
COPY package*.json ./

RUN npm install
# If you are building your code for production

# Bundle app source
COPY . .

CMD [ "node", "server/index.js" ]