import Api from '@/services/Api'

export default {
  getUserById(userId) {
    return Api().get(`users/${userId}`, {
      params: {
        user_id: userId
      }
    })
  }
}
