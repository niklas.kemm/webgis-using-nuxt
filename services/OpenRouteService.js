import axios from 'axios'

export default {
  GeocodeSearch(SearchString) {
    return axios
      .create({
        baseURL: 'https://api.openrouteservice.org'
      })
      .get(`geocode/search`, {
        params: {
          api_key: '5b3ce3597851110001cf6248df0c7ee0069e490b8435f8a09b516103',
          text: SearchString
        }
      })
  },
  GeocodeReverse(lon, lat) {
    return axios
      .create({
        baseURL: 'https://api.openrouteservice.org'
      })
      .get(`geocode/reverse`, {
        params: {
          api_key: '5b3ce3597851110001cf6248df0c7ee0069e490b8435f8a09b516103',
          'point.lon': lon,
          'point.lat': lat
        }
      })
  },
  Directions(transport, from, to) {
    return axios
      .create({
        baseURL: 'https://api.openrouteservice.org'
      })
      .get(`v2/directions/${transport}/`, {
        params: {
          api_key: '5b3ce3597851110001cf6248df0c7ee0069e490b8435f8a09b516103',
          start: from,
          end: to
        }
      })
  }
}
